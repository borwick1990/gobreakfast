package sample

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

// 定義 interface 方便 mock 實作
type UserI interface {
	setAge(age int)
	setName(name string)
	getProfile() string
}

// 被 mock 的 struct
type User struct {
	Name string
	Age  int
}

func (u *User) getProfile() string {
	return "Name is: " + u.Name + " ,age is:" + strconv.Itoa(u.Age)
}

func (u *User) setAge(age int) {
	u.Age = age
}

func (u *User) setName(name string) {
	u.Name = name
}

// 建立 mock struct
type MockUser struct {
	mock.Mock
}

func (m *MockUser) setAge(age int) {
	// 代表有呼叫到，所以即使沒回傳也必須存在
	m.Called()
}
func (m *MockUser) setName(name string) {
	// 代表有呼叫到，所以即使沒回傳也必須存在
	m.Called()
}
func (m *MockUser) getProfile() string {
	args := m.Called()
	// return value
	return args.String(0)
}

// 呼叫方法，我們要 mock 的東西在這邊會呼叫掉，傳入 interface 方便測試
func IntroDuce(user UserI) string {
	user.setAge(18)
	user.setName("Steven")
	return user.getProfile()
}

func TestMock(t *testing.T) {
	// 實例 mocker，設定預期會被呼叫方法＆次數，回傳什麼
	user := new(MockUser)
	user.On("setAge").Once()
	user.On("setName").Once()
	user.On("getProfile").Return("mock test").Once()

	exp := IntroDuce(user)
	assert.Equal(t, exp, "mock test")

	// 關閉 mock 並驗證預期呼叫的有沒有做到
	user.AssertExpectations(t)
}

func TestAssert(t *testing.T) {
	assert.Equal(t, 123, 123)

	assert.NotEqual(t, 123, 456)

	assert.Nil(t, nil)
}
