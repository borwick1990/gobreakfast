package sample

func GormCreate() {
	//CREATE
	item := Item{Name: "漢堡", Category: "漢堡類", Price: 190}
	conn.Debug().Create(&item)
	employee := Employee{Name: "早餐店ＣＥＯ"}
	conn.Debug().Create(&employee)
	ingredients := Ingredients{Name: "漢堡包", Category: "麵包類", Cost: 190, Stock: 100}
	conn.Debug().Create(&ingredients)
	orders := Orders{EmployeeId: 1, Total: 1, State: "準備中", OrderSource: "Ubereats", OrderType: "外送", TableNumber: 1}
	conn.Debug().Create(&orders)
}
