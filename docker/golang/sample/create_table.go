package sample

import (
	"fmt"
)

func CreateTable() {
	//產生table
	conn.Debug().AutoMigrate(&Item{})
	migrator := conn.Migrator()
	//判斷有沒有table存在
	has := migrator.HasTable(&Item{})
	if !has {
		fmt.Println("table not exist")
	}
}
