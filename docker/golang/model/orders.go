package model

import (
	"time"
)

type Orders struct {
	ID          uint
	EmployeeId  int `gorm:"ForeignKey:EmployeeId"`
	Total       int
	State       string
	OrderSource string
	OrderType   string
	TableNumber int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (Orders) TableName() string {
	return "orders"
}
