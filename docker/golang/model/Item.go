package model

import (
	"time"
)

type Item struct {
	ID        uint
	Name      string
	Category  string
	Price     int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (Item) TableName() string {
	return "item"
}
