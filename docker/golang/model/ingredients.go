package model

import (
	"time"
)

type Ingredients struct {
	ID        uint
	Name      string
	Category  string
	Cost      int
	Stock     int
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (Ingredients) TableName() string {
	return "ingredients"
}
