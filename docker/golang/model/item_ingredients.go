package model

import (
	"time"
)

type ItemIngredients struct {
	ID         uint
	ItemId     int `gorm:"ForeignKey:ItemId"`
	Ingredient int `gorm:"ForeignKey:IngredientId"`
	Optional   string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func (ItemIngredients) TableName() string {
	return "item_ingredients"
}
