package model

import (
	"time"
)

type Employee struct {
	ID        uint       `json:"-"`
	Name      string     `json:"name"`
	CreatedAt *time.Time `gorm:"type:timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP" json:"created_at,omitempty"`
	UpdatedAt *time.Time `gorm:"type:timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP" json:"updated_at,omitempty"`
}

func (Employee) TableName() string {
	return "employee"
}
