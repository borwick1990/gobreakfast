package model

import (
	"time"
)

type OrdersItem struct {
	ID        uint
	OrderId   int `gorm:"ForeignKey:OrdersId"`
	ItemId    int `gorm:"ForeignKey:ItemId"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (OrdersItem) TableName() string {
	return "orders_item"
}
