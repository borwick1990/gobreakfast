package model

import (
	"fmt"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	UserName     string = "root"
	Password     string = "root"
	Addr         string = "docker.for.mac.localhost"
	Port         int    = 3306
	Database     string = "go_breakfast"
	MaxLifetime  int    = 30
	MaxOpenConns int    = 30
	MaxIdleConns int    = 30
)

func ConnectDB() {
	//組合sql連線字串
	addr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True", UserName, Password, Addr, Port, Database)

	//連接MySQL
	conn, conn_err := gorm.Open(mysql.Open(addr), &gorm.Config{})

	if conn_err != nil {
		fmt.Println("connection to mysql failed:", conn_err)
	}

	//連接DB
	db, db_err := conn.DB()
	if db_err != nil {
		fmt.Println("get db failed:", db_err)
	}

	//設定ConnMaxLifetime/MaxIdleConns/MaxOpenConns
	db.SetConnMaxLifetime(time.Duration(MaxLifetime) * time.Second)
	db.SetMaxIdleConns(MaxIdleConns)
	db.SetMaxOpenConns(MaxOpenConns)
}
