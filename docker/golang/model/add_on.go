package model

import (
	"time"
)

type AddOn struct {
	ID         uint
	ItemId     int `gorm:"ForeignKey:ItemId"`
	Ingredient int `gorm:"ForeignKey:IngredientId"`
	Optional   string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func (AddOn) TableName() string {
	return "add_on"
}
