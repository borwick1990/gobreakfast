package lib

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("health", func(ctx *gin.Context) {
		// ctx.String(http.StatusOK, "DB connect:"+db_result)
		ctx.String(http.StatusOK, "health check ok")
	})
	return r
}

func Run() {
	r := SetupRouter()
	//http port:8000
	r.Run(":8000")
}
