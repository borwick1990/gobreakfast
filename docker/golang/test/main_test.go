package test

import (
	"breakfast/hello/model"
	"testing"

	"github.com/stretchr/testify/suite"
	"gorm.io/driver/sqlite" // Sqlite driver based on GGO
	"gorm.io/gorm"
)

// 測試主父層，初始化共用東東，例如 db
type MainTest struct {
	suite.Suite
}

func (m *MainTest) SetupTest() {
	m.SetTestDb()
}

// 建立 sqlite db
func (m *MainTest) SetTestDb() {
	// use sqlite memory db
	dsn := "file::memory:?cache=shared"
	db, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to intit test database")
	}

	// Migrate table schema
	db.AutoMigrate(&model.AddOn{})
	db.AutoMigrate(&model.Employee{})
	db.AutoMigrate(&model.Ingredients{})
	db.AutoMigrate(&model.ItemIngredients{})
	db.AutoMigrate(&model.Item{})
	db.AutoMigrate(&model.OrdersItem{})
	db.AutoMigrate(&model.Orders{})
}

// 讓 suit 跑測試
func TestRun(t *testing.T) {
	suite.Run(t, new(ApiTest))
}
