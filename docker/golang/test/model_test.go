package test

import (
	"fmt"

	"github.com/stretchr/testify/suite"
)

type ModelTest struct {
	suite.Suite
	MainTest
}

// 各 test.go 之下直接定義 test method 即可
func (m *ModelTest) TestM() {
	fmt.Println("TestM")
}
