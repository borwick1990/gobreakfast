package test

import (
	"net/http"
	"net/http/httptest"

	"breakfast/hello/lib"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type ApiTest struct {
	suite.Suite
	MainTest
}

// 各 test.go 之下直接定義 test method 即可
func (m *ApiTest) TestHealth() {
	t := m.Suite.T()
	router := lib.SetupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/health", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}
