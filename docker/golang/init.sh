#!/bin/bash

# Check if mysql ready to connect
FILE=index.html

while ! test -f "$FILE";
do
  echo "Waiting for database connection..."
  wget mysql:3306;

  sleep 2
done;

# run app to build go
/go/bin/hello