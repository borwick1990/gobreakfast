package main

import (
	"breakfast/hello/lib"
	"breakfast/hello/model"
)

func main() {
	model.ConnectDB()
	lib.Run()
}
